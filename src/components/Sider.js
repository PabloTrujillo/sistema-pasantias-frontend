/**
 * Created by chalosalvador on 3/1/20
 */
import React from "react";
import Navigation from "./Navigation";

const Sider = () => (
  <div>
    <Navigation />
  </div>
);

export default Sider;
